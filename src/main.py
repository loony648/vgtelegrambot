import asyncio
import logging
from aiogram import Bot, Dispatcher, types
from datetime import date, timedelta 
from dateutil import parser
from src.volgatech_api import volgatech_api
import locale

# bot token
BOT_TOKEN = ""
# set locale
locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')
# Configure logging
logging.basicConfig(level=logging.INFO)
# Initialize bot and dispatcher
bot = Bot(token=BOT_TOKEN)
dp = Dispatcher(bot)

# Set locale
buttons = [['Расписание', 'Github']]


@dp.message_handler(commands=['start', 'help'])
async def start_handler(event: types.Message):
    with open('./img/vg_tyan.jpg', 'rb') as photo:
        keyboard_reply = types.ReplyKeyboardMarkup(
            keyboard=buttons,
            resize_keyboard=True
        )
        await event.reply_photo(
            photo,
            "Привет!" +
            " " +
            "Чтобы посмотреть расписание БИ-12 на неделю" +
            "," +
            "нажми на кнопку ниже :)",
            reply_markup=keyboard_reply
        )

@dp.message_handler()
async def send_message(event: types.Message):
    if event.text == 'Github':
        with open('./img/github.jpg', 'rb') as photo:
            await event.reply_photo(photo, caption="github.com/loony648")
    elif event.text == 'Расписание':
        vgApi = volgatech_api.VolgatechApi()
        userAuthInfo = vgApi.auth("", "")
        vgApi.setTokenInHeader(userAuthInfo['accessToken'])
        userInfo = vgApi.getUserInfo()
        userId = userInfo[0]['studentId']

        schedule = list()
        today = date.today()
        start = today - timedelta(days=today.weekday())
        end = start + timedelta(days=6)
        schedule = vgApi.getSchedule(str(userId), start=start, end=end)
        finalScheduleParsed = ""
        for scheduleDays in schedule:
            scheduleParsed = ""
            for scheduleDay in scheduleDays['events']:
                building = scheduleDay['building']
                category = scheduleDay['category']
                when = parser.parse(scheduleDay['date']).strftime(u'%A')
                description = scheduleDay['description']
                teacherName = scheduleDay['fio']
                groupName = scheduleDay['fullDescription']
                subGroup = scheduleDay['subGroup']
                personId = scheduleDay['personId']
                roomNum = scheduleDay['room']
                timeBegin = scheduleDay['timeBegin']
                timeEnd = scheduleDay['timeEnd']
                typeWorkName = scheduleDay['typeWorkName']
                weekNumber = scheduleDay['weekNumber']
                weekTypeName = scheduleDay['weekTypeName']
                htmlColorCode = scheduleDay['htmlColorCode']
                url = scheduleDay['url']
                scheduleParsed += '\nПара: {}\nТип пары: {}\nВремя пары: {} - {}\nКабинет: {}\nКорпус: {}\n'.format(description, typeWorkName, timeBegin, timeEnd, roomNum, building)
            finalScheduleParsed += "\nДень недели: " + when + '\n' + scheduleParsed + "\n------------------------"
        await event.reply(finalScheduleParsed)
    else:
        await event.reply("Неизвестная команда :c")


async def main():
    try:
        await dp.start_polling()
    finally:
        await bot.close()

asyncio.run(main())