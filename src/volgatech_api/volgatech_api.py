import json
import requests
from datetime import date 
from requests.exceptions import HTTPError


class VolgatechApi:
    baseURL = 'https://api.volgatech.net'

    methodsMap = {
        'login': '/api/Auth/GetToken',
        'refresh': '/api/Auth/RefreshToken',
        'studentInfo': '/api/Student/GetProfiles',
        'studentProfile': '/api/Student/GetInfo',
        'refreshToken': '/api/Auth/RefreshToken',
        'getNews': '/api/News/1/0',
        'exams': '/api/Exams',
        'getCalendar': '/api/Calendar',
        'getNotifications': '/api/News/2/0',
        'getCourses': '/api/CourseSemestrs',
        'getCalendarEvents': '/api/Calendar/GetEventExists/',
        'getProgress': '/api/ProgressSemestr/GetProgressSemestrWithAtt',
        'getGrants': '/api/Grant/',
        'socialRequest': '/api/socialRequest/',
        'getPortfolioDirections': '/api/Dict/GetDictPortfolioDirections',
        'getDictPortfolioWorkTypes': '/api/Dict/GetDictPortfolioWorkTypes',
        'getDictPortfolioWorkSubTypes': '/api/Dict/GetDictPortfolioWorkSubTypes',
        'getPortfolio': '/api/Portfolio/',
        'getFilePortfolio': '/api/Files/portfolio',
        'addPortfolioWork': '/api/Portfolio',
        'updatePortfolio': '/api/Portfolio',
        'getPGASPeriod': '/api/PgasRequest/GetPGASRequestPeriodAndRequest/',
        'addPGASRequest': '/api/PgasRequest',
        'getPGASRequest': '/api/PgasRequest/GetPrintableRequest',
        'removePortfolioWorkFile': '/api/Files/portfolio',
        'removePortfolioWork': '/api/Portfolio',
        'updatePortfolioWork': '/api/Portfolio',
        'getMiddleGrants': '/api/Grant/payments',
        'getDictRequestCategories': '/api/Dict/GetDictRequestCategories',
        'getDictRoomBuilding': '/api/Dict/GetDictRoomBuilding',
        'getDictRooms': '/api/Dict/GetDictRooms',
        'postStudentRequest': '/api/Requests/PostStudentRequest',
        'getStudentRequestList': '/api/Requests/GetStudentRequestList',
        'deleteStudentRequest': '/api/Requests/DeleteStudentRequest',
        'deleteStudentRequestAttach': '/api/Files/studentrequests',
        'returnStudentRequest': '/api/Requests/ReturnStudentRequest',
        'sendStudentRequest': '/api/Requests/SendStudentRequest',
        'putStudentRequest': '/api/Requests/PutStudentRequest',
        'getDictRequestStatuses': '/api/Dict/GetDictRequestStatuses',
        'getStudentRequestAttachments': '/api/Files/studentrequests',
        'getrequestAttachmentAllowedFormats': '/api/Dict/GetrequestAttachmentAllowedFormats',
        'getPageHelp': '/api/PageHelp/student',
        'getAllHelp': '/api/PageHelp/student',
        'getComments': '/api/Requests/GetComments',
        'sendComment': '/api/Requests/PostStudentRequestComment'
    }

    headers = {
        "Content-Type": "application/json",
        "Accept": "text/plain"
    }

    def __init__(self) -> object:
        pass

    @classmethod
    def setTokenInHeader(self, token: str) -> None:
        self.headers['Authorization'] = "Bearer " + token

    @classmethod
    def auth(self, login: str, password: str = "Qwerty1") -> json:
        """
        Authorizing in API

        login - personal student number(starts with s)\n
        password - password(by default is Qwerty1)
        """
        self.data = {
            "login": login,
            "password": password
        }
        try:
            req = requests.post(
                self.baseURL + self.methodsMap['login'],
                headers=self.headers,
                json=self.data
            )
        except requests.exceptions.HTTPError as err:
            raise SystemExit(err)

        return req.json()

#        try:
#            req = requests.post(
#                self.baseURL + self.methodsMap['login'],
#                headers=self.headers,
#                json=self.data
#            )
#        except requests.exceptions.HTTPError as err:
#            raise SystemExit(err)
#
#        try:
#            resp = req.json()
#            self.accessToken = resp['accessToken']
#            self.refreshToken = resp['refreshToken']
#            self.expiresIn = resp['expiresIn']
#            self.expiresDateTime = resp['expiresDateTime']
#        except ValueError:
#            print('ValueError. Json in wrong format!')
#            raise
#        except KeyError as err:
#            print("KeyError. Can't get " + str(err) + " from AUTH response")
#            raise
#        else:
#            self.setTokenInHeader(self.accessToken)

    @classmethod
    def getUserInfo(self) -> json:
        """
        Getting user info from API
        """
        try:
            req = requests.get(
                self.baseURL + self.methodsMap['studentInfo'],
                headers=self.headers
            )
        except HTTPError:
            print("Can't get user info from API. Check connection.")
        else:
            return req.json()

    @classmethod
    def getUserProfile(self, userId: int) -> json:
        """
        Getting user profile info from API

        userId - student identificator in str
        """
        try:
            req = requests.get(
                self.baseURL + self.methodsMap['studentProfile'] + '/' + userId,
                headers=self.headers
            )
        except HTTPError:
            print("Can't get UserProfile info from API. Check connection.")
        else:
            return req.json()

    @classmethod
    def getSchedule(self, userId: str, start: date, end: date) -> json:
        """
        userId - student identificator in str
        #TODO: make normal translation!
        start - date from which day we should get schedule
        end - date from which day we should get schedule
        """
        try:
            req = requests.get(
                self.baseURL + self.methodsMap['getCalendar'] + '/' + userId + '/' + str(start) + '/' + str(end) + '/' + 'Timetable,Event,Private',
                headers=self.headers
            )
        except HTTPError:
            print("Can't get Schedule from API. Check connection.")
        else:
            return req.json()

    @classmethod
    def getNews(self) -> json:
        """
        Get news from API
        """
        try:
            req = requests.get(
                self.baseURL + self.methodsMap['getNews'],
                headers=self.headers
            )
        except HTTPError:
            print("Can't get news from API. Check connection.")
        else:
            return req.json()

    @classmethod
    def getGrants(self, userId: int) -> json:
        """
        Get grants for student
        userId - student identificator in str
        """
        try:
            req = requests.get(
                self.baseURL + self.methodsMap['getGrants'] + '/' + userId,
                headers=self.headers,
            )
        except HTTPError:
            print("Can't get grants info from API. Check connection.")
        else:
            return req.json()

    @classmethod
    def getCourses(self, userId: int) -> json:
        """
        Get avaliable courses for student
        userId - student identificator in str
        """
        try:
            req = requests.get(
                self.baseURL + self.methodsMap['getCourses'] + '/' + userId,
                headers=self.headers,
            )
        except HTTPError:
            print("Can't get courses info from API. Check connection.")
        else:
            return req.json()

    @classmethod
    def getDictRoomBuilding(self) -> json:
        try:
            req = requests.get(
                self.baseURL + self.methodsMap['getDictRoomBuilding'],
                headers=self.headers,
            )
        except HTTPError:
            print("Can't get dictRoomsBuildings from API. Check connection.")
        else:
            return req.json()

    @classmethod
    def getDictRooms(self) -> json:
        try:
            req = requests.get(
                self.baseURL + self.methodsMap['getDictRooms'],
                headers=self.headers,
            )
        except HTTPError:
            print("Can't get dictRooms info from API. Check connection.")
        else:
            return req.json()

    @classmethod
    def getExams(self, userId: str, semestr: str) -> json:
        """
        userId - student identificator in str
        semestr - number of semester in str
        """
        try:
            req = requests.get(
                self.baseURL + self.methodsMap['exams'] + '/' + userId + '/' + semestr,
                headers=self.headers
            )
        except HTTPError:
            print("Can't get exam info from API. Check connection.")
        else:
            return req.json()
