__author__ = 'Nikita Anisimov'
__email__ = 'anisimov0324@gmail.com'
__version__ = '0.0.1'

from . import volgatech_api

__all__ = (
    'volgatech_api',
)
